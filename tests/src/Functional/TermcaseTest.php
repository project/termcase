<?php

namespace Drupal\Tests\termcase\Functional;

use Drupal\Tests\taxonomy\Functional\TaxonomyTestBase;

/**
 * Class TermcaseTest. The base class for testing term cases.
 */
class TermcaseTest extends TaxonomyTestBase {

  /**
   * Vocabulary for testing.
   *
   * @var \Drupal\taxonomy\VocabularyInterface
   */
  protected $vocabulary;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['taxonomy', 'termcase'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test term cases.
   */
  public function testTermCases() {
    // Log in as an admin user.
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Create a new vocabulary.
    $this->vocabulary = $this->createVocabulary();

    // Configure the case for the vocabulary.
    $config = \Drupal::configFactory()->getEditable('termcase.settings');
    $config->set('vocabularies', [
      $this->vocabulary->id() => [
        'option' => TERMCASE_LOWERCASE,
      ],
    ]);
    $config->save();

    // Check the status of the page.
    $this->drupalGet("admin/structure/taxonomy/manage/{$this->vocabulary->id()}/add");
    $this->assertSession()->statusCodeEquals(200);

    // Create a new taxonomy term.
    $term = $this->createTerm($this->vocabulary, [
      'name' => 'Apple',
    ]);

    // The label should be lowercase.
    $this->assertEquals($term->label(), 'apple');

    // Change the case for the vocabulary.
    $config = \Drupal::configFactory()->getEditable('termcase.settings');
    $config->set('vocabularies', [
      $this->vocabulary->id() => [
        'option' => TERMCASE_UPPERCASE,
      ],
    ]);
    $config->save();

    // Create another taxonomy term.
    $term = $this->createTerm($this->vocabulary, [
      'name' => 'pear',
    ]);

    // The label should be uppercase.
    $this->assertEquals($term->label(), 'PEAR');
  }

}
