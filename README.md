# Termcase

The Termcase module gives site administrators the option
to specify case-formatting on the terms within a vocabulary.

There are five options available:

1. No formatting
2. Ucfirst: convert the first character of the term to uppercase
3. Lowercase: convert all characters of the term to lowercase
4. Uppercase: convert all characters of the term to uppercase
5. Propercase: convert the first character of earch word to uppercase

You can define these settings per vocabulary. Apart from the included
formatting options,you can use a hook in your own module to alter the
terms just before they are saved,so if needed you can add your own formatting
on top of the already applied formatting.The module also enables you to update
all terms in an existing vocabulary.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/termcase).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/termcase).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

When the module is enabled, Termcase settings should appear
on Vocabulary edit page.


## Maintainers

- Baris - [BarisW](https://www.drupal.org/u/barisw)
- Jack Over - [scuba_fly](https://www.drupal.org/u/scuba_fly)
- Sonam Singh - [sonamsingh](https://www.drupal.org/u/sonamsingh)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)

