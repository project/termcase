<?php

/**
 * @file
 * Hooks specific to the Termcase module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter string conversion before it is saved.
 *
 * @param string $converted_string
 *   The string that is about to be saved after the conversion.
 * @param \Drupal\taxonomy\TermInterface $term
 *   The term to update.
 * @param int $case
 *   Defines the required case formatting. Possible options:
 *   - TERMCASE_NONE
 *   - TERMCASE_UCFIRST
 *   - TERMCASE_LOWERCASE
 *   - TERMCASE_UPPERCASE
 *   - TERMCASE_PROPERCASE.
 */
function hook_termcase_convert_string_alter(&$converted_string, $term, $case) {
  $converted_string = str_replace(' ', '', $converted_string);
}

/**
 * @} End of "addtogroup hooks".
 */
