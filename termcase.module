<?php

/**
 * @file
 * Contains termcase.module.
 *
 * This module prevents users to use different cases on terms.
 * Site-admins now can make sure all terms in a vocabulary begin with an
 * uppercase or that they are all formatted to uppercase / lowercase.
 */

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\VocabularyInterface;

define('TERMCASE_NONE', 0);
define('TERMCASE_UCFIRST', 1);
define('TERMCASE_LOWERCASE', 2);
define('TERMCASE_UPPERCASE', 3);
define('TERMCASE_PROPERCASE', 4);

/**
 * Implements hook_form_FORM_ID_alter().
 */
function termcase_form_taxonomy_term_form_alter(&$form, FormStateInterface $form_state) {
  /** @var \Drupal\taxonomy\TermInterface $entity */
  $entity = $form_state->getFormObject()->getEntity();

  $edit_link = t('You can change this setting on the <a href="@vocabulary_edit_link">vocabulary settings page</a>.', [
    '@vocabulary_edit_link' => Url::fromRoute('entity.taxonomy_vocabulary.edit_form', [
      'taxonomy_vocabulary' => $entity->bundle(),
    ])->toString(),
  ]);

  $config = \Drupal::configFactory()->getEditable('termcase.settings')->get('vocabularies', []);
  $termcase = !empty($config[$entity->bundle()]) ? $config[$entity->bundle()]['option'] : TERMCASE_NONE;

  switch ($termcase) {
    case TERMCASE_UCFIRST:
      $form['name']['widget'][0]['value']['#description'] = t('Please note: the first letter of this term will be converted to <em>Uppercase</em>.') . ' ' . $edit_link;
      break;

    case TERMCASE_LOWERCASE:
      $form['name']['widget'][0]['value']['#description'] = t('Please note: the term will be converted to <em>lowercase</em>.') . ' ' . $edit_link;
      break;

    case TERMCASE_UPPERCASE:
      $form['name']['widget'][0]['value']['#description'] = t('Please note: the term will be converted to <em>UPPERCASE</em>.') . ' ' . $edit_link;
      break;

    case TERMCASE_PROPERCASE:
      $form['name']['widget'][0]['value']['#description'] = t('Please note: the term will be converted to <em>Proper Case</em>.') . ' ' . $edit_link;
      break;

  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds the termcase selection dropdown to the vocabulary form.
 */
function termcase_form_taxonomy_vocabulary_form_alter(&$form, FormStateInterface $form_state) {
  $config = \Drupal::config('termcase.settings')->get('vocabularies', []);
  $vocab_machine_name = $form['vid']['#default_value'];

  $form['termcase']['termcase_settings'] = [
    '#title' => t('Term case settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#weight' => 2,
    'termcase_options' => [
      '#title' => t('Convert terms to this case'),
      '#type' => 'select',
      '#options' => [
        TERMCASE_NONE => t('No conversion'),
        TERMCASE_UCFIRST => t('Convert the first character to uppercase'),
        TERMCASE_LOWERCASE => t('Convert all characters to lowercase'),
        TERMCASE_UPPERCASE => t('Convert ALL CHARACTERS TO UPPERCASE'),
        TERMCASE_PROPERCASE => t('Convert the first character of each word to uppercase'),
      ],
      '#default_value' => !empty($config[$vocab_machine_name]) ? $config[$vocab_machine_name]['option'] : TERMCASE_NONE,
      '#description' => t('This applies to all terms that are added to this vocabulary.'),
    ],
  ];

  $form['submit']['#weight'] = 4;

  // These settings only apply on existing vocabularies.
  if (isset($form['vid'])) {
    $form['delete']['#weight'] = 5;

    $form['termcase']['termcase_settings']['termcase_options']['#description'] = t('Note: existing terms will not be changed.');

    $form['termcase']['termcase_settings']['termcase_update_terms'] = [
      '#title' => t('Convert the existing terms in this vocabulary immediately'),
      '#type' => 'checkbox',
      '#default_value' => !empty($config[$vocab_machine_name]) ? $config[$vocab_machine_name]['update'] : FALSE,
    ];
  }

  // Defining custom submit handler.
  $form['actions']['submit']['#submit'][] = 'termcase_custom_form_submit';
}

/**
 * Submit callback to save Termcase settings with the Vocabulary.
 */
function termcase_custom_form_submit($form, FormStateInterface $form_state) {
  $vocab_machine_name = $form_state->getValue('vid');

  $config = \Drupal::configFactory()->getEditable('termcase.settings');
  $vocabularies = $config->get('vocabularies', []);
  $vocabularies[$vocab_machine_name] = [
    'option' => (int) $form_state->getValue('termcase_options'),
    'update' => (bool) $form_state->getValue('termcase_update_terms'),
  ];
  $config->set('vocabularies', $vocabularies)->save();

  if ($form_state->getValue('termcase_update_terms') && $form_state->getValue('termcase_options')) {
    /** @var \Drupal\taxonomy\TermInterface[] $terms */
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
      'vid' => $vocab_machine_name,
    ]);

    if ($terms) {
      $batch = [
        'title' => t('Updating terms...'),
        'operations' => [],
      ];

      foreach ($terms as $term) {
        $batch['operations'][] = [
          '\Drupal\termcase\TermCaseUpdateTerms::updateTerm',
          [$term, $form_state->getValue('termcase_options')],
        ];
      }

      batch_set($batch);
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 *
 * Remove Termcase settings when vocabularies are deleted.
 */
function termcase_taxonomy_vocabulary_delete(VocabularyInterface $entity) {
  $config = \Drupal::configFactory()->getEditable('termcase.settings');
  $vocabularies = $config->get('vocabularies', []);

  if (isset($vocabularies[$entity->id()])) {
    unset($vocabularies[$entity->id()]);
    $config->set('vocabularies', $vocabularies)->save();
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function termcase_taxonomy_term_presave(TermInterface $term) {
  $vocab_machine_name = $term->bundle();

  $config = \Drupal::config('termcase.settings');
  $vocabularies = $config->get('vocabularies', []);

  if (isset($vocabularies[$vocab_machine_name])) {
    $termcase_mode = $vocabularies[$vocab_machine_name]['option'];

    $term_name = _termcase_convert_string_to_case($term, $termcase_mode);
    $term->setName($term_name);
  }
}

/**
 * Helper function to convert the current string to the specified case.
 *
 * @param \Drupal\taxonomy\TermInterface $term
 *   The term to update.
 * @param string $case
 *   Case of the vocabulary, the term belongs to.
 *
 * @return string
 *   The term converted in proper case.
 */
function _termcase_convert_string_to_case($term, $case = TERMCASE_NONE) {
  switch ($case) {
    case TERMCASE_UCFIRST:
      $converted_string = Unicode::ucfirst($term->label());
      break;

    case TERMCASE_LOWERCASE:
      $converted_string = mb_strtolower($term->label());
      break;

    case TERMCASE_UPPERCASE:
      $converted_string = mb_strtoupper($term->label());
      break;

    case TERMCASE_PROPERCASE:
      $words = explode(' ', $term->label());
      foreach ($words as $key => $word) {
        $words[$key] = Unicode::ucfirst($word);
      }
      $converted_string = implode(' ', $words);
      break;

    default:
      $converted_string = $term->label();
      break;
  }

  // Allow modules to change the string before saving it.
  \Drupal::moduleHandler()->alter('termcase_convert_string', $converted_string, $term, $case);

  return $converted_string;
}

/**
 * Implements hook_entity_type_build().
 */
function termcase_entity_type_build(array &$entity_types) {
  if (isset($entity_types['taxonomy_vocabulary'])) {
    $entity_types['taxonomy_vocabulary']->setListBuilderClass('Drupal\termcase\TermCaseVocabularyListBuilder');
  }
}

/**
 * Function to fetch termcase mode.
 *
 * @param int $mode
 *   Id of the mode.
 *
 * @return string
 *   Short description of the mode.
 */
function termcase_get_mode($mode) {
  switch ($mode) {
    case TERMCASE_UCFIRST:
      $termcase_mode = t('First character uppercase');
      break;

    case TERMCASE_LOWERCASE:
      $termcase_mode = t('All characters lowercase');
      break;

    case TERMCASE_UPPERCASE:
      $termcase_mode = t('All characters uppercase');
      break;

    case TERMCASE_PROPERCASE:
      $termcase_mode = t('First character of each word uppercase');
      break;

    default:
      $termcase_mode = t('None');
  }

  return $termcase_mode;
}
