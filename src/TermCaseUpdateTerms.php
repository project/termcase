<?php

namespace Drupal\termcase;

/**
 * Class, which contains the callbacks of a batch process.
 */
class TermCaseUpdateTerms {

  /**
   * Batch process callback.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to update.
   * @param int $case
   *   The case to convert the terms to.
   * @param array $context
   *   Sandbox context.
   */
  public static function updateTerm($term, $case, array &$context) {
    $converted_term = _termcase_convert_string_to_case($term, $case);

    if ($converted_term !== $term->label()) {
      $term->setName($converted_term);
      $term->save();
    }
  }

}
